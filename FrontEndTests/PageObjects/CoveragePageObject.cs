﻿using FrontEndTests.DataEntities;
using OpenQA.Selenium;
using System.Collections.Generic;
using FrontEndTests.Hooks;

namespace FrontEndTests.PageObjects
{
    /// <sumary>
    /// Coverage Page Object
    /// </sumary>
    public class CoveragePageObject
    {
        //The URL of the Truphone Coverage to be opened in the browser
        private const string TruphoneCoverageUrl = "https://www.truphone.com/things/plans/coverage/";

        //The Selenium web driver to automate the browser
        private readonly IWebDriver _webDriver;

        public CoveragePageObject(IWebDriver webDriver)
        {
            _webDriver = webDriver;
        }

        //Elements
        private IWebElement SearchInput => _webDriver.FindElement(By.Id("search"));
        private IWebElement FirstCountryName => _webDriver.FindElement(By.CssSelector(".jss20 > h4"));
        private IWebElement SecondCountryName => _webDriver.FindElement(By.CssSelector(".jss25 > h4"));
        private IWebElement ThirdCountryName => _webDriver.FindElement(By.CssSelector(".jss32 > h4"));
        private IWebElement FourthCountryName => _webDriver.FindElement(By.CssSelector(".jss39 > h4"));
        private IWebElement FifthCountryName => _webDriver.FindElement(By.CssSelector(".jss44 > h4"));
        private IWebElement SixthCountryName => _webDriver.FindElement(By.CssSelector(".jss47 > h4"));
        private IWebElement SeventhCountryName => _webDriver.FindElement(By.CssSelector(".jss52 > h4"));
        private IWebElement EighthCountryName => _webDriver.FindElement(By.CssSelector(".jss55 > h4"));
        private IWebElement FirstCountryCoverageType => _webDriver.FindElement(By.CssSelector(".jss22"));
        private IWebElement SecondCountryCoverageType => _webDriver.FindElement(By.CssSelector(".jss27"));
        private IWebElement ThirdCountryCoverageType => _webDriver.FindElement(By.CssSelector(".jss34"));
        private IWebElement FourthCountryCoverageType => _webDriver.FindElement(By.CssSelector(".jss41"));
        private IWebElement FifthCountryCoverageType => _webDriver.FindElement(By.CssSelector(".jss44 > p"));
        private IWebElement SixthCountryCoverageType => _webDriver.FindElement(By.CssSelector(".jss49"));
        private IWebElement SeventhCountryCoverageType => _webDriver.FindElement(By.CssSelector(".jss52 > p"));
        private IWebElement EighthCountryCoverageType => _webDriver.FindElement(By.CssSelector(".jss57"));
        public IWebElement OnlyOneItemList => _webDriver.FindElement(By.XPath("/html/body/div[2]/div/div[2]/div/div/div[1]/div[1]/div/div/div[2]/h4"));

        public void OpenCoveragePage()
        {
            _webDriver.Navigate().GoToUrl(TruphoneCoverageUrl);
            AgreeCookiesAndClosePopUp();
        }

        private void AgreeCookiesAndClosePopUp()
        {
            var acceptedCookie = By.CssSelector("div.menu-MuiBox-root.jss104.jss97.jss103 > button");
            WaitElements.WaitUntilElementClickable(_webDriver, acceptedCookie);
            _webDriver.FindElement(acceptedCookie).Click();

            var popUp = By.CssSelector("div.toast-popup-container.is-visible > div > button");
            WaitElements.WaitUntilElementClickable(_webDriver, popUp);
            _webDriver.FindElement(popUp).Click();
        }

        public void SearchCountry(string country)
        {
            SearchInput.Click();
            SearchInput.SendKeys(country);
            SearchInput.SendKeys(Keys.Enter);
        }

        public void CountriesListIsDisplay()
        {
            var list = By.CssSelector("#truphone-for-things-coverage > div.menu-MuiBox-root.jss17.sc-dRPiIx.cpAyqT");
            WaitElements.WaitUntilElementVisible(_webDriver, list);
        }

        public List<Country> GetNameEightFirstCountries()
        {
            var listCountries = new List<Country>()
            {
                new Country{name=FirstCountryName.Text},
                new Country{name=SecondCountryName.Text},
                new Country{name=ThirdCountryName.Text},
                new Country{name=FourthCountryName.Text},
                new Country{name=FifthCountryName.Text},
                new Country{name=SixthCountryName.Text},
                new Country{name=SeventhCountryName.Text},
                new Country{name=EighthCountryName.Text}
            };

            return listCountries;
        }

        public List<Country> GetCoverageTypeEightFirstCountries()
        {
            var listCountries = new List<Country>()
            {
                new Country{name=FirstCountryName.Text, coverageType=FirstCountryCoverageType.Text},
                new Country{name=SecondCountryName.Text, coverageType=SecondCountryCoverageType.Text},
                new Country{name=ThirdCountryName.Text, coverageType=ThirdCountryCoverageType.Text},
                new Country{name=FourthCountryName.Text, coverageType=FourthCountryCoverageType.Text},
                new Country{name=FifthCountryName.Text, coverageType=FifthCountryCoverageType.Text},
                new Country{name=SixthCountryName.Text, coverageType=SixthCountryCoverageType.Text},
                new Country{name=SeventhCountryName.Text, coverageType=SeventhCountryCoverageType.Text},
                new Country{name=EighthCountryName.Text, coverageType=EighthCountryCoverageType.Text}
            };

            return listCountries;
        }
    }
}
