﻿Feature: Coverage 
	Page for showing our coverage arround the world

	Background: 

	Given It has access the coverage page

Scenario: The list of 8 first countries
	When the list of countries covergage is display
	Then the list should be contain 
	| name			 |
	| AFGHANISTAN    |
	| ÅLAND ISLANDS  |
	| ALBANIA        |
	| ALGERIA        |
	| AMERICAN SAMOA |
	| ANDORRA        |
	| ANGOLA         |
	| ANGUILLA       |

Scenario: This list with 8 first countries two countries are not covered 
	When the list of countries covergage is display
	Then It should be have two countires not covered 
	| name           | coverageType |
	| AFGHANISTAN    | 2G, 3G, 4G   |
	| ÅLAND ISLANDS  | 2G, 3G       |
	| ALBANIA        | 2G, 3G, 4G   |
	| ALGERIA        | 2G, 3G, 4G   |
	| AMERICAN SAMOA | Not covered  |
	| ANDORRA        | 2G, 3G, 4G   |
	| ANGOLA         | Not covered  |
	| ANGUILLA       | 2G, 3G, 4G   |

Scenario Outline: Searching by an existing country
	When It has search by an existing <country>
	Then the info about country searching should be displayed

	Examples: 
	| country  |
	| Brazil   |
	| Portugal |
	| Japan    |