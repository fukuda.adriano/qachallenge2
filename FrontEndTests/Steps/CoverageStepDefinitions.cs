﻿using FluentAssertions;
using FrontEndTests.DataEntities;
using FrontEndTests.Drivers;
using FrontEndTests.PageObjects;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace FrontEndTests.Steps
{
    [Binding]
    public class CoverageStepDefinitions
    {
        private readonly ScenarioContext _scenarioContext;
        private readonly CoveragePageObject _coveragePageObject;

        public CoverageStepDefinitions(ScenarioContext scenarioContext, BrowserDriver browserDriver)
        {
            _scenarioContext = scenarioContext;
            
            _coveragePageObject = new CoveragePageObject(browserDriver.Current);
        }


        [Given(@"It has access the coverage page")]
        public void GivenItHasAccessTheCoveragePage()
        {
            _coveragePageObject.OpenCoveragePage();
        }
        
        [When(@"the list of countries covergage is display")]
        public void WhenTheListOfCountriesCovergageIsDisplay()
        {
            _coveragePageObject.CountriesListIsDisplay();
        }
        
        [Then(@"the list should be contain")]
        public void ThenTheFirstCountriesShouldBe(Table table)
        {
            var countries = _coveragePageObject.GetNameEightFirstCountries();

            table.CompareToSet<Country>(countries);
        }

        [Then(@"It should be have two countires not covered")]
        public void ThenItShouldBeHaveTwoCountiresNotCovered(Table table)
        {
            var countries = _coveragePageObject.GetCoverageTypeEightFirstCountries();

            table.CompareToSet<Country>(countries);

        }

        [When(@"It has search by an existing (.*)")]
        public void WhenItHasSearchByAnExistingCountry(string country)
        {
            _scenarioContext["country"] = country;
            _coveragePageObject.SearchCountry(country);
        }

        [Then(@"the info about country searching should be displayed")]
        public void ThenTheInfoAboutCountrySearchingShouldBeDisplayed()
        {
            var expectedCountryName = (string)_scenarioContext["country"];
            var atualCountryName = _coveragePageObject.OnlyOneItemList.Text;

            expectedCountryName.Should().Equals(atualCountryName);
        }


    }
}
